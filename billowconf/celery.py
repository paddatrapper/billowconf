###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from __future__ import absolute_import
import os
import logging
from celery import Celery

logger = logging.getLogger(__name__)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'billowconf.settings')

app = Celery('billowconf')

app.config_from_object('django.conf:settings', namespace='CELERY')
logger.info('Celery app configured')
app.autodiscover_tasks()
logger.info('Celery tasks discovered. Running...')
