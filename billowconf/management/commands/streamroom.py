###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.core.management.base import BaseCommand
from billowconf.models import Room
from billowconf.utils import stream_webrtc_room

class Command(BaseCommand):
    help = 'Streams to a Janus room'

    def add_arguments(self, parser):
        parser.add_argument('room_id',
                             help='The database ID of the room to stream to')

    def handle(self, *args, **options):
        room_id = options['room_id']
        room = Room.objects.filter(id=room_id)
        if not room.exists():
            self.stderr.write(f'Room {room_id} does not exist')
            return
        room = room.get()
        stream_webrtc_room(room)
