###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase
from billowconf.permissions import IsAdminOrIsSelf, IsAdminOrReadOnly, IsAdminOrAuthenticatedReadOnly, ReadOnly

class ReadOnlyTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = '/api/users/1/'
        cls.factory = RequestFactory()

    def test_unauthenticated(self):
        request = self.factory.get(self.path)
        request.user = AnonymousUser()
        check = ReadOnly()
        result = check.has_permissions(request, None)
        self.assertTrue(result)

    def test_authenticated(self):
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        check = ReadOnly()
        result = check.has_permissions(request, None)
        self.assertTrue(result)

    def test_no_post(self):
        request = self.factory.post(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        check = ReadOnly()
        result = check.has_permissions(request, None)
        self.assertFalse(result)

class IsAdminOrIsSelfTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = '/api/users/1/'
        cls.factory = RequestFactory()

    def test_unauthenticated(self):
        request = self.factory.get(self.path)
        request.user = AnonymousUser()
        check = IsAdminOrIsSelf()
        result = check.has_object_permissions(request, None, None)
        self.assertFalse(result)

    def test_is_staff(self):
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob', is_staff=True)
        request.user = user
        check = IsAdminOrIsSelf()
        result = check.has_object_permissions(request, None, None)
        self.assertTrue(result)

    def test_is_self(self):
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        check = IsAdminOrIsSelf()
        result = check.has_object_permissions(request, None, user)
        self.assertTrue(result)

    def test_is_not_self(self):
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        other = User.objects.create_user(username='other')
        request.user = user
        check = IsAdminOrIsSelf()
        result = check.has_object_permissions(request, None, other)
        self.assertFalse(result)

class IsAdminOrReadOnlyTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = '/api/users/1/'
        cls.factory = RequestFactory()

    def test_unauthenticated(self):
        request = self.factory.get(self.path)
        request.user = AnonymousUser()
        check = IsAdminOrReadOnly()
        result = check.has_permission(request, None)
        self.assertTrue(result)

    def test_unauthenticated_post(self):
        request = self.factory.post(self.path)
        request.user = AnonymousUser()
        check = IsAdminOrReadOnly()
        result = check.has_permission(request, None)
        self.assertFalse(result)

    def test_admin_post(self):
        request = self.factory.post(self.path)
        user = User.objects.create_user(username='jacob', is_staff=True)
        request.user = user
        check = IsAdminOrReadOnly()
        result = check.has_permission(request, None)
        self.assertTrue(result)

class IsAdminOrAuthenticatedReadOnlyTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = '/api/users/1/'
        cls.factory = RequestFactory()

    def test_unauthenticated(self):
        request = self.factory.get(self.path)
        request.user = AnonymousUser()
        check = IsAdminOrAuthenticatedReadOnly()
        result = check.has_permission(request, None)
        self.assertFalse(result)

    def test_authenticated(self):
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        check = IsAdminOrAuthenticatedReadOnly()
        result = check.has_permission(request, None)
        self.assertTrue(result)

    def test_authenticated_post(self):
        request = self.factory.post(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        check = IsAdminOrAuthenticatedReadOnly()
        result = check.has_permission(request, None)
        self.assertFalse(result)

    def test_admin_post(self):
        request = self.factory.post(self.path)
        user = User.objects.create_user(username='jacob', is_staff=True)
        request.user = user
        check = IsAdminOrAuthenticatedReadOnly()
        result = check.has_permission(request, None)
        self.assertTrue(result)
