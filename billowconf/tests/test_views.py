###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.contrib.auth.models import User
from django.core import mail
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from billowconf.views import reset_password_request

class TestResetPasswordRequest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = '/api/password/reset/request/'
        cls.factory = APIRequestFactory()

    def test_send_email(self):
        request = self.factory.post(self.path, {'email': 'test@example.com'})
        user = User.objects.create_user('test@example.com', email='test@example.com')
        request.user = user
        response = reset_password_request(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'BillowConf password reset requested')

    def test_send_email_not_matched(self):
        request = self.factory.post(self.path, {'email': 'noexist@example.com'})
        user = User.objects.create_user('test@example.com', email='test@example.com')
        force_authenticate(request, user)
        response = reset_password_request(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)
