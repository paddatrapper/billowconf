###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.contrib.auth.models import User
from django.core import mail
from django.test import TestCase
from billowconf.serializers import UserSerializer

class TestResetPasswordRequest(TestCase):
    def test_create_user(self):
        serializer = UserSerializer(data={
            'name': 'Test User', 
            'email': 'test@example.com',
        })
        serializer.is_valid()
        user = serializer.create(serializer.validated_data)
        created_user = User.objects.get(username='test@example.com')
        self.assertEqual(user, created_user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'BillowConf account created')
