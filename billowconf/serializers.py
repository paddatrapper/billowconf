###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import logging
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework import serializers
from .models import Conference, Room, Talk, ConferenceManagementPerson, ResetPasswordToken, create_reset_token

logger = logging.getLogger(__name__)

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'id', 'name']

class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='last_name')

    def create(self, validated_data):
        validated_data['username'] = validated_data['email']
        user = User.objects.create_user(**validated_data)
        logger.info(f'Creating user {user.get_full_name}')
        serializer = EmailSerializer(data={'email': user.email})

        token = create_reset_token(serializer)
        if token:
            logger.info('Sending password rest email to new user')
            subject = 'BillowConf account created'
            sender = settings.EMAIL_FROM
            receiver = user.email
            context = {
                'name': token.user.get_full_name(),
                'reset_link': f'{settings.HOST_DOMAIN}/accounts/reset/{token.key}',
                'organization': settings.EMAIL_NAME,
            }
            message = render_to_string('billowconf/account_created_email.txt',
                                       context=context)
            send_mail(subject, message, sender, [receiver], fail_silently=False)
        return user

    def update(self, instance, validated_data):
        instance.username = validated_data['email']
        instance.email = validated_data['email']
        instance.last_name = validated_data['last_name']
        instance.is_staff = validated_data['is_staff']
        instance.save()
        logger.info(f'Updated user {instance.get_full_name}')
        return instance;

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'name', 'email', 'is_staff']
        read_only_fields = ['username']

class ConferenceManagementPersonSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.get_full_name', read_only=True)

    class Meta:
        model = ConferenceManagementPerson
        fields = ['url', 'id', 'user', 'username', 'conference', 'management_id']


class ConferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conference
        fields = [
            'url',
            'id',
            'name',
            'irc_server',
            'start_date',
            'end_date',
            'placeholder'
        ]

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = [
            'url',
            'id',
            'name',
            'conference',
            'irc_channel',
            'webrtc_channel',
            'active_webrtc_feed'
        ]
        read_only_fields = ['webrtc_channel']

class TalkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talk
        fields = [
            'url',
            'id',
            'name',
            'description',
            'start',
            'duration',
            'presenters',
            'room',
            'recorded_content'
        ]

class TokenSerializer(serializers.ModelSerializer):
    token = serializers.CharField(source='key')
    class Meta:
        model = ResetPasswordToken
        fields = ['token']

class PasswordTokenSerializer(serializers.Serializer):
    password = serializers.CharField(style={'input_type': 'password'})
    token = serializers.CharField()

class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()
